PI generator torque controller in region 1 (variable speed, variable power), starting at istate = 3
K =   0.108313E+07 [Nm/(rad/s)^2]
PI generator torque controller in region 2 (constant speed, variable power)
I =   0.307683E+08 [kg*m^2]
Kp =   0.135326E+08 [Nm/(rad/s)]
Ki =   0.303671E+07 [Nm/rad]
PI pitch angle controller in region 3 (constant speed, constant power)
Kp =   0.249619E+01 [rad/(rad/s)]
Ki =   0.120122E+01 [rad/rad]
K1 =        7.30949 [deg], K2 =     1422.81187 [deg^2] (dq/dtheta =     -176.48944 kNm/deg)
Additional terms due to the Aerodynamic damping
Kp2 =   0.240394E-01 [rad/(rad/s)]
Ko1 =       -1.69769 [deg], Ko2 =      -15.02688 [deg^2] (dq/domega =      243.08924 kNm/(rad/s))
